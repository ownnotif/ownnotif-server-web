# OwnNotif

Take privacy seriously and own your push notifications ✊📢! OwnNotif is a secure, free (libre), open source, self-host push notifications server/clients.

## Server Web Client Management

Allow to manage an instance of OwnNotif server for users/applications/API tokens/etc.
