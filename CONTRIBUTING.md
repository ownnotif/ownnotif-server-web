# Contributing to OwnNotif Server Web Management

Contribution are welcome from anyone for anything. Just make sure you read and understand the [C4 process](https://rfc.ownnotif.com/spec-2/C4) that we use for contribution and start creating merge requests!

## Coding Style

For styling, we use [Prettier](https://prettier.io/) on Javascript, JSON, Markdown and CSS. As of now, this is sufficient and the project is configured with a pre-commit hook. The hook is found within `package.json` and uses the plugin [Husky](https://github.com/typicode/husky) and [lint-staged](https://github.com/okonet/lint-staged). Merge request should not be accepted without being put through Prettier first.
